package sortersTesterClasses;

import java.util.ArrayList;
import java.util.Comparator;

import interfaces.Sorter;
import sorterClasses.AbstractSorter;
import sorterClasses.BubbleSortSorter;

public class IntegerArrayTester {

    static Integer[] intArr = {5, 9, 20, 22, 20, 5, 4, 13, 17, 8, 22, 1, 3, 7, 11, 9, 10};
    
    public static void main(String[] args) {
       // test("Sorting Using DefaultComp", null);
    	showArray("Original Array", intArr);
        test("Sorting Using IntegerComparator1", new IntegerComparator1());
        test("Sorting Using IntegerComparator2", new IntegerComparator2());

    }
    
    
    public static void test(String msg, Comparator<Integer> cmp) {



        
        System.out.println("\n\n*******************************************************");
        System.out.println("*** " + msg + "  ***");
        System.out.println("*******************************************************");
        

            Sorter<Integer> sorter = new BubbleSortSorter<Integer>();
            sorter.sort(intArr, cmp);
            showArray(sorter.getName() + ": ", intArr); 
        
}
    

    private static void showArray(String msg, Integer[] a) {
        System.out.print(msg); 
        for (int i=0; i<a.length; i++) 
            System.out.print("\t" + a[i]); 
        System.out.println();
    }

}
