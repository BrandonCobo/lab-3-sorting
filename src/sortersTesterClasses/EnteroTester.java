package sortersTesterClasses;

import java.util.ArrayList;
import java.util.Comparator;

import interfaces.Sorter;
import sorterClasses.AbstractSorter;
import sorterClasses.BubbleSortSorter;

//Exercise 1
public class EnteroTester {


    
    public static void main(String[] args) {
        test("Sorting Using DefaultComp", null);
        

    }
    
    
    public static void test(String msg, Comparator<Entero> cmp) {
        Entero[] enteroArray = new Entero[5];
        enteroArray[0] = new Entero(5);
        enteroArray[1] = new Entero(6);
        enteroArray[2] = new Entero(3);
        enteroArray[3] = new Entero(1);
        enteroArray[4] = new Entero(9);

        
        System.out.println("\n\n*******************************************************");
        System.out.println("*** " + msg + "  ***");
        System.out.println("*******************************************************");
        

            Sorter<Entero> sorter = new BubbleSortSorter<Entero>(); 
            sorter.sort(enteroArray, cmp);
            showArray(sorter.getName() + ": ", enteroArray); 

}
    

    private static void showArray(String msg, Entero[] a) {
        System.out.print(msg); 
        for (int i=0; i<a.length; i++) 
            System.out.print("\t" + a[i]); 
        System.out.println();
    }

}
